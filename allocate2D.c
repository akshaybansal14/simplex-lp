#include <stdio.h>
#include <stdlib.h>


float **allocate2D(int m, int n){

	int i;
	float **M;
	M=(float **)calloc(m,sizeof(float*));
	if(M == NULL){
		fprintf(stderr,"\nOut of Memory\n");
		exit(1);
	}

	for(i=0;i<m;i++){
		M[i]=(float*)calloc(n,sizeof(float));
		if(M[i]==NULL){
			fprintf(stderr,"\nOut of Memory\n");
			exit(1);
		}
	}

	return(M);
}

