#include <stdlib.h>

void free2D(float **M, int m){

	int i;
	for(i=0;i<m;i++)
		free(M[i]);

	free(M);

	return;
}
