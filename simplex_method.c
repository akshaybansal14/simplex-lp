#include <stdio.h>
#include <math.h>
#include "isBounded.h"
#include "print_tableau.h"


void simplex_method(float **TBL, float *R, int *B, int *NB, int m, int n){

	int i,j,min_E, index_E, min_L,index_L,temp;
	float min_limit,check_limit,factor;
	
	do{


		/* To find the leaving and entering variable using Bland's rule */	
		min_E=m+n; //check for termination condition 
		for(i=0;i<n;i++){
			if(R[i]>0 && NB[i]<min_E){
				index_E=i;
				min_E=NB[i];
			}
		}
		if(min_E==m+n)
			break;
		if(!isBounded(TBL,m,index_E)){
			printf("\nUnBounded\n");
			return;
		}
	
		min_limit=INFINITY; //check for termination/exit
		for(i=0;i<m;i++){
			if(TBL[i][index_E]<0){
				check_limit=-TBL[i][n]/TBL[i][index_E];
				if(check_limit<=min_limit){
					if(check_limit<min_limit){
						index_L=i;
						min_L=B[i];
					}
					else{
						if(B[i]<min_L){
							index_L=i;
							min_L=B[i];
						}
					}
					min_limit=check_limit;
				}
			}
		}

		factor=-TBL[index_L][index_E];
		for(j=0;j<=n;j++){
			if(j!=index_E)
				TBL[index_L][j]=TBL[index_L][j]/factor;		
		}
		TBL[index_L][index_E]=-1/factor;

		i=0;
		while(i<m){
			if(i!=index_L){
				for(j=0;j<=n;j++){
					if(j!=index_E)
						TBL[i][j]=TBL[i][j]+TBL[i][index_E]*TBL[index_L][j];
				}
				TBL[i][index_E]=TBL[i][index_E]*TBL[index_L][index_E];
			}
			i++;
		}
		for(j=0;j<=n;j++){
			if(j!=index_E)
				R[j]=R[j]+R[index_E]*TBL[index_L][j];
		}
		R[index_E]=R[index_E]*TBL[index_L][index_E];


		temp=B[index_L];
		B[index_L]=NB[index_E];
		NB[index_E]=temp;


		printf("\n");
		print_tableau(TBL,R,B,NB,m,n);

	}while(1);
	
	return;
}

