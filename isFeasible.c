#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "simplex_method.h"
#include "allocate2D.h"
#include "allocate1D.h"
#include "free2D.h"
#include "print_tableau.h"
#define EPSILON 0.00001

void isFeasible(float **A, float *b, float *c, int m, int n){
	
	int i,j,*B,*NB,status; 
	float **TBL,*R;
	

	B=(int*)calloc(m,sizeof(int));
	NB=(int*)calloc(n+m,sizeof(int));
	TBL=allocate2D(m,n+m+1);
	R=allocate1D(n+m+1);	

	/* Allocating and intializing basic and non-basic variables */
	if(B==NULL || NB==NULL){
		fprintf(stderr,"\nOut of Memory\n");
		exit(1);
	}
	for(i=0;i<m;i++)
		B[i]=i+n+m;
	for(i=0;i<n+m;i++)
		NB[i]=i;

	/* Initializing the Simplex Tableau */
	for(i=0;i<m;i++){
		for(j=0;j<n;j++)
			TBL[i][j]=-A[i][j];
	}
	for(i=0;i<m;i++){
		for(j=0;j<m;j++){
			if(i==j)
				TBL[i][j+n]=-1;
			else
				TBL[i][j+n]=0;
		}
	}

	for(i=0;i<m;i++)
		TBL[i][n+m]=b[i];

	//Added
	for(i=0;i<m;i++){
		if(b[i]<0){
			for(j=0;j<=m+n;j++)
				TBL[i][j]=-TBL[i][j];
		}	
	}


	for(j=0;j<=n+m;j++){
		R[j]=0;
		for(i=0;i<m;i++)
			R[j]+=-TBL[i][j];
	}

	printf("Auxillary\n");
	print_tableau(TBL,R,B,NB,m,n+m);
	
	simplex_method(TBL,R,B,NB,m,n+m);
	if(fabs(R[n+m])>EPSILON){
		printf("\nInfeasible System\n");
		status=1;
	}
	else{
		status=0;
	}

	printf("Display\n");
	print_tableau(TBL,R,B,NB,m,n+m);
	if(!status){
		/* Coefficients of extra variables = zero */
		for(j=0;j<n+m;j++){
			R[j]=0;
			if(NB[j]>=n+m){
				for(i=0;i<m;i++)
					TBL[i][j]=0;
			}
		}
		R[n+m]=0;
		for(j=0;j<n+m;j++){
			if(NB[j]<n)
				R[j]=c[NB[j]];
			for(i=0;i<m;i++){
				if(B[i]<n)
					R[j]+=c[B[i]]*TBL[i][j];
			}
		}
		for(i=0;i<m;i++){
			if(B[i]<n)
				R[n+m]+=c[B[i]]*TBL[i][n+m];
		}
		print_tableau(TBL,R,B,NB,m,n+m);
		simplex_method(TBL,R,B,NB,m,m+n);
	}

	free(B); free(NB);
	free2D(TBL,m); free(R);
	return;

}


