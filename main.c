#include <stdio.h>
#include <stdlib.h>
#include "allocate2D.h"
#include "allocate1D.h"
#include "isFeasible.h"
#include "free2D.h"


int main(){

	int m,n,i,j;
	float **A, *b, *c;

	/* To input the elements of the constraint matrix A, bound vector b and objective function c */  
	printf("Enter the size(m,n) of the constraint matrix A (Ax <= b):");
	scanf("%d %d",&m, &n);
	A=allocate2D(m,n+m);
	b=allocate1D(m);
	c=allocate1D(n+m);

	printf("\nInput constraint matrix A (Ax <= b)\n");
	for(i=0;i<m;i++){
		for(j=0;j<n;j++){
			printf("Enter A[%d][%d]: ",i,j);
			scanf("%f",&A[i][j]);
		}
	}
	printf("\nInput the bound vector b (Ax <= b)\n");
	for(i=0;i<m;i++){
		printf("Enter b[%d]: ",i);
		scanf("%f",&b[i]);
	}

	printf("\nInput the objective function vector c (max cTx)\n");
	for(i=0;i<n;i++){
		printf("Enter c[%d]: ",i);
		scanf("%f",&c[i]);
	}
	
	isFeasible(A,b,c,m,n);

	//simplex_method(A,b,c,m,n);

		
	free2D(A,m);free(b);free(c);

	return(0);

}

