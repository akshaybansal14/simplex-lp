CC = gcc
CFLAGS = -c -Wall
LDFLAGS = -lm
OBJECTS = main.o simplex_method.o isFeasible.o	isBounded.o print_tableau.o allocate2D.o allocate1D.o free2D.o

prog: $(OBJECTS)
	$(CC) $(OBJECTS) -o simplex $(LDFLAGS)

main.o : main.c isFeasible.h allocate2D.h allocate1D.h free2D.h 
	$(CC) $(CFLAGS) main.c 

simplex_method.o : simplex_method.c simplex_method.h isBounded.h print_tableau.h
	$(CC) $(CFLAGS) simplex_method.c

isFeasible.o : isFeasible.c isFeasible.h allocate2D.h allocate1D.h print_tableau.h simplex_method.h free2D.h
	$(CC) $(CFLAGS) isFeasible.c 

isBounded.o : isBounded.c isBounded.h  
	$(CC) $(CFLAGS) isBounded.c

print_tableau.o : print_tableau.c print_tableau.h
	$(CC) $(CFLAGS) print_tableau.c
 
allocate2D.o : allocate2D.c allocate2D.h
	$(CC) $(CFLAGS) allocate2D.c
 
allocate1D.o : allocate1D.c allocate1D.h
	$(CC) $(CFLAGS) allocate1D.c
 
free2D.o : free2D.c free2D.h
	$(CC) $(CFLAGS) free2D.c

clean:
	rm -f $(OBJECTS) 
