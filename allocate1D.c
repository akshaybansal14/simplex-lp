#include <stdio.h>
#include <stdlib.h>

float *allocate1D(int m){

	float *M;
	M=(float*)calloc(m,sizeof(float));
	if(M==NULL){
		fprintf(stderr,"\nOut of Memory\n");
		exit(1);
	}

	return(M);

} 
