#include <stdio.h>

void print_tableau(float **TBL, float *R, int *B, int *NB, int m, int n){

	int i,j;

	for(i=0;i<m;i++){
		printf("x[%d] = %f ",B[i],TBL[i][n]);
		for(j=0;j<n;j++)
			printf("%f x[%d] ",TBL[i][j],NB[j]);
		printf("\n");
	}

	printf("OBJ = %f ",R[n]);
	for(i=0;i<n;i++)
		printf("%f x[%d] ",R[i],NB[i]);
	printf("\n");

	return;

}

